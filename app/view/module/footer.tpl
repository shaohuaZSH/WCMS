
<!-- 配置文件 -->
<script type="text/javascript"
	src="./static/public/ueditor/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript"
	src="./static/public/ueditor/ueditor.all.js"></script>
{literal}

<script type="text/javascript">
	var ue = UE.getEditor('container',{
	    autoHeightEnabled: false
	});

	$('#form_date').datetimepicker({
		language : 'zh-CN',
		format : 'yyyy-mm-dd hh:ii',
		weekStart : 1,
		todayBtn : 0,
		autoclose : 1,
		todayHighlight : 1,
		startView : 2,
		minView : 2,
		forceParse : 0
	});
</script>
{/literal}
