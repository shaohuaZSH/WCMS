{include file="news/header.tpl"}

{literal}
<script type="text/javascript" language="javascript">  
        $(document).ready(function() {  
        	$("#chk_all").click( 
       			 function(){ 
 					if(this.checked){ 
 					 $("input[name='chk_list']").prop('checked', true) 
 					 }else{ 
 							 
 					$("input[name='chk_list']").removeAttr("checked");
 					} 
 					} 
 					
 					);
                  });   
            
          

         

            function del(id){  
              if(!confirm("确认删除?")){
                  return;
                 }
          	  $.post("./index.php?member/remove",{uid:id},function(data){
               	
           	     if(data.message!="success"){
           	    	alert(data.message);
           	    	return;
           	     }else{
            		  $("#member"+id).fadeOut();

               	     }
             },"json")
          } 
            
            
    </script>

{/literal}

<!-- 头部// -->
<!-- 头部// -->
{include file="news/top.tpl"}


{include file="news/nav.tpl"}
	<!-- start: Content -->
			<div id="content" class="">
			
						
			<div class="row-fluid">


<div class="well"><!-- Default panel contents -->


<div class="box-content">
 <div method="get" class="form-inline suoding">
<select name="type" id="type" class="span1">
	<option value="uid">UID</option>
	<option value="username" >账号</option>
	<option value="mobile_phone">手机</option>
	<option value="real_name" selected>名字</option>
	<option value="area">地区</option>

</select>
	<div class="input-append">

<input value="{$search}" type="text" id="value" name="value" class="input-medium">
 <button
	 class="btn " onclick="sub()"> <i class="icon-search"></i></div>
	 </button> <span class="glyphicon glyphicon-user"></span>	




</div>


<table class="table  table-hover">
	<tr>
	<th>UID</th>
	<th class="span3">真实姓名</th>
	
	
			<th class="span2">用户组</th>
	
		<th class="span1">
		积分
		</th>
		
		<th class="span2">手机</th>
		<th class="span2">登录时间</th>
		<th class="span2"></th>
				<th class="span2" colspan="2">有  <b>{$totalnum}</b> 条记录</th>
		
	</tr>




	{section name=l loop=$news }
	<tr id="member{$news[l].uid}">
	<td>{$news[l].uid}</td>
		<td id="{$news[l].uid}_pd">
			
			<a href="javascript:edit({$news[l].uid})" 
			data-toggle="tooltip"
			data-placement="right"
			title="{$news[l].username}的信息"
			data-content="UID:{$news[l].uid}"
			class="info">{$news[l].real_name}</a>
			
{if $news[l].verify==1}<span class="label label-success">实</span>{/if}
	{if $news[l].status=='禁用'}
			<span class="label">禁</span>
			{elseif $news[l].status=='离职'}
		    &nbsp;&nbsp;<span class="label">离</span>
			{/if}		
			
		
			 </td>

		

		
		<td id="{$news[l].uid}_realname">{$news[l].name} </td>

		
		<td><a href="./index.php?coupons/user/?uid={$news[l].uid}">{$news[l].coupons}</a>
			</td>
		<td><span id="{$news[l].uid}_sj">{$news[l].mobile_phone}</span></td>
		
		
		<td><small>{$news[l].lastlogin|wtime}</small></td>
		<td>
	
		</td>
<td> <a href="javascript:del({$news[l].uid})">x</a></td>
<td></td>
	</tr>
	{/section}

</table>

<div class="pagination pagination-centered">
<ul id="pager"></ul></div>

</div>




</div>
<script src="./static/public/layer/layer.min.js" ></script>


{literal}
<script type='text/javascript'>



$(document).ready(function(){

	  $(".info").bind("mouseover",function(){
		    $(this).popover("show");
			   })
	  $(".info").bind("mouseout",function(){
		    $(this).popover("hide");
			   })
  $("input[name='value']").keyup(function(event){
            if(event.which==13){
                 sub();
            }
		  })
})


function sub(){
    var type=$("#type").val();
    var value=$("#value").val();
	location.href='./index.php?member/csearch/?type='+type+"&value="+value;
}

function edit(id){
$.layer({
    type: 2,
    shadeClose: true,
    title: false,
    closeBtn: [0, true],
    shade: [0.8, '#666'],
    border: [0],
    offset: ['20px',''],
    area: ['700px', ($(window).height() - 50) +'px'],
    iframe: {src: './index.php?member/edit/?id='+id,
        scrolling: 'no'
         }
}) 
}

function addarea(id){
	$.layer({
	    type: 2,
	    shadeClose: true,
	    title: false,
	    closeBtn: [0, true],
	    shade: [0.8, '#666'],
	    border: [0],
	    offset: ['20px',''],
	    area: ['700px', ($(window).height() - 150) +'px'],
	    iframe: {src: './index.php?stores/add/?uid='+id,
	        scrolling: 'no'
	         }
	}) 
	}
function stores(id){
	$.layer({
	    type: 2,
	    shadeClose: true,
	    title: false,
	    closeBtn: [0, true],
	    shade: [0.8, '#666'],
	    border: [0],
	    offset: ['20px',''],
	    area: ['800px', '650px'],
	    iframe: {src: './index.php?stores/getstorebyuid/?uid='+id,
	        scrolling: 'no'
	         }
	}) 
	}

function add(id){
	$.layer({
	    type: 2,
	    shadeClose: true,
	    title: false,
	    closeBtn: [0, true],
	    shade: [0.8, '#666'],
	    border: [0],
	    offset: ['20px',''],
	    area: ['700px', ($(window).height() - 50) +'px'],
	    iframe: {src: './index.php?member/add',
	        scrolling: 'no'
	         }
	}) 
	}



var options = {
currentPage: {/literal}{$num.current}{literal},
totalPages: {/literal}{$num.page}{literal},
numberOfPages:5,
bootstrapMajorVersion:3,
pageUrl: function(type, page, current){
    return './index.php?member/listing/?groupid={/literal}{$groupid.id}&verify={$verify.id}&{literal}&p='+page;
}
}
$('#pager').bootstrapPaginator(options);
</script>
{/literal}
{include file="news/footer.tpl"}
